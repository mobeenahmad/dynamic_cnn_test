"""
Generic setup of the data sources and the model training. 

Based on:
    https://github.com/fchollet/keras/blob/master/examples/mnist_mlp.py
and also on 
    https://github.com/fchollet/keras/blob/master/examples/mnist_cnn.py

"""

#import keras
from keras.datasets       import mnist, cifar10
from keras.models         import Sequential, Model
from keras.layers         import Conv2D, MaxPooling2D, Dense, Dropout, Flatten, Concatenate, BatchNormalization, Activation
from keras.utils.np_utils import to_categorical
from keras.callbacks      import EarlyStopping, Callback, TensorBoard
from keras                import backend as K,optimizers, regularizers, Input
from keras.utils          import plot_model
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session
import datetime 
import os
import logging
from time import time

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)
set_session(sess)
K.set_image_dim_ordering('tf')


def get_mnist(nb_classes):
    """Retrieve the MNIST dataset and process the data."""
    
    # Input image dimensions
    img_rows, img_cols = 28, 28
    
    # Get the data, shuffled and split between train and test sets
    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    
    if K.image_data_format() == 'channels_first':
        x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
        x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
        input_shape = (1, img_rows, img_cols)
    else:
        x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
        x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
        input_shape = (img_rows, img_cols, 1)
    
    x_train = x_train.astype('float32')
    x_test  = x_test.astype('float32')
    x_train /= 255
    x_test  /= 255

    # convert class vectors to binary class matrices
    y_train = to_categorical(y_train, nb_classes)
    y_test  = to_categorical(y_test,  nb_classes)

    return (input_shape, x_train, x_test, y_train, y_test)

def add_conv_block(block_input=(28,28,1),layers=2,neurons=64,kernel=(3,3),activation="relu",padding="same",pool_size=(2,2),pool_padding="same",pool_strides=(2,2)):
    for i in range(0,layers):
        next_input=Conv2D(neurons, kernel_size=kernel, activation=activation, padding=padding)(block_input)
    
    output=MaxPooling2D(pool_size=pool_size, padding=pool_padding, strides=pool_strides)(next_input)
    return output

def compile_model(hyperparams, nb_classes, input_shape):
    """Compile a sequential model.

    Args:
        genome (dict): the parameters of the genome

    Returns:
        a compiled network.

    """
    # Get our network parameters.
    nb_blocks    = hyperparams["nb_blocks"]   # e.g. 5                               (Number of Blocks)
    block_sizes  = hyperparams["block_sizes"] # e.g. [2,2,4,4,4] for VGG 16          (Block Depth)
    nb_neurons   = hyperparams['nb_neurons']  # e.g. [64,128,256,512,512] for VGG 16 (Layer Width in Each Block)
    activation   = hyperparams['activation']  # e.g. "relu"
    optimizer    = hyperparams['optimizer' ]  # e.g. "adamax"

    img_input = Input(shape=input_shape)

    next_input=img_input
    for i in range(1,nb_blocks):
        next_input=add_conv_block(next_input,block_sizes[i],nb_neurons[i],kernel=(3,3),activation=activation,padding="same")

    output = Flatten()(next_input)
    output = Dense(4096, activation=activation)(output)
    output = Dropout(0.5)(output)
    output = Dense(nb_classes, activation='softmax')(output)

    model = Model(inputs=[img_input], outputs=[output])
    model.compile(loss='categorical_crossentropy', optimizer=optimizer,metrics=['accuracy'])
    return model

def train_and_score(hyperparams,nb_classes,batch_size,epochs):
    """
        Load Dataset
        Compile Model
        Train the model
        return test accuracy
    """
    """
    Args:
        hyperparams (dict): the parameters of the network
    """

    
    
    input_shape, x_train, x_test, y_train, y_test = get_mnist(nb_classes)

    model = compile_model(hyperparams, nb_classes, input_shape)
   
    model.fit(x_train, y_train,
        batch_size=batch_size,
        epochs=epochs,  
        verbose=1,
        validation_data=(x_test, y_test),
        callbacks=[])

    score = model.evaluate(x_test, y_test, verbose=0)

    K.clear_session()
    
    return score[1]


def main():

    batch_size = 512
    epochs     = 4
    nb_classes = 10   # Number of Classes in MNIST dataset

    hyperparams={
        'activation': 'softplus',
        'optimizer': 'rmsprop',
        'nb_blocks': 2,
        'nb_neurons': [256, 512],
        'block_sizes': [4, 4]
    }

    accuracy=train_and_score(hyperparams,nb_classes,batch_size,epochs)

    print("Accuracy: %s",accuracy)


if __name__=='__main__':
    main()