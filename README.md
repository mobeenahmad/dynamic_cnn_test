# dynamic_cnn_test

This program creates a CNN model using Keras functional API based on provided hyper-parameters.

Change following parameters to create a new CNN model.


```
hyperparams={
        'activation': 'softplus',       # e.g. "relu" in VGG 16
        'optimizer': 'rmsprop',	        # e.g. "sgd" in VGG 16
        'nb_blocks': 2,			        # e.g. 5 for VGG 16			            (Number of Blocks)
        'nb_neurons': [256, 512],	    # e.g. [64,128,256,512,512] for VGG 16  (Layer Width in Each Block)
        'block_sizes': [4, 4]		    # e.g. [2,2,4,4,4] for VGG 16           (Block Depth)
    }


batch_size = 512
epochs     = 4
nb_classes = 10   # Number of Classes in MNIST dataset
```

See [requirements.txt](requirements.txt) for dependencies
